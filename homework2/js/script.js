/* Размеры, виды начинок и добавок */
const sizes = {
  SIZE_SMALL: 'small',
  SIZE_LARGE: 'large',
};

const sizesProperties = {
  SIZE_SMALL: {
    price: 50,
    cal: 20,
  },
  SIZE_LARGE: {
    price: 100,
    cal: 40,
  }
};

const availableStuffings = {
  STUFFING_CHEESE: 'cheese',
  STUFFING_SALAD: 'salad',
  STUFFING_POTATO: 'potato',
};

const stuffingProperties = {
  STUFFING_CHEESE: {
    price: 10,
    cal: 20
  },
  STUFFING_SALAD: {
    price: 20,
    cal: 5
  },
  STUFFING_POTATO: {
    price: 15,
    cal: 10
  },
};

const availableToppings = {
  TOPPING_MAYO: 'mayo',
  TOPPING_SPICE: 'spice'
};

const toppingProperties = {
  TOPPING_MAYO: {
    price: 20,
    cal: 15
  },
  TOPPING_SPICE: {
    price: 15,
    cal: 0
  }
};


class Hamburger {
  constructor (size, stuffing){
   try {
     if (arguments.length !== 2) {
       throw new Error('Invalid parameter number')
   }
     if (size !== 'large' && size !== 'small') {
       throw new Error('Invalid size')
     }
     if (stuffing !== 'cheese' && stuffing !== 'salad' && stuffing !== 'potato') {
       throw new Error('Invalid stuffing')
     }
  } catch (error) {
     alert((`Error  ${error.name}: ${error.message}`))
   }
  this._size = getKeyByValue(sizes, size);
  this._stuffing = getKeyByValue(availableStuffings, stuffing);
  this._topping = [];
  }
  get size() {
    return this._size
  }

  set size(value) {
  this._size = getKeyByValue(sizes, value);

  }
  get stuffing() {
    return this._size
  }

  set stuffing(value) {
  this._stuffing = getKeyByValue(availableStuffings, value);

  }

  get topping() {
    return this._topping
  }

   set topping(topping) {
    try {
      if (topping !== 'mayo' && topping !== 'spice') {
        throw new Error('Invalid topping')
      }
      if (this._topping.indexOf(getKeyByValue(availableToppings, topping)) === -1) {
        this._topping = this._topping.concat(getKeyByValue(availableToppings, topping));
      } else {
        throw new Error("Dublicate topping");
    }
  } catch (error) {
      alert(`Ошибка  ${error.name}: ${error.message}`);
    }
 }

 set removeTopping(topping) {
   try {
     if (topping !== 'mayo' && topping !== 'spice') {
       throw new Error('Invalid topping')
     }
    const toppingIndex = this._topping.indexOf(getKeyByValue(availableToppings, topping));
if (toppingIndex !== -1) {
    this._topping.splice(toppingIndex, 1);
  } else {
  throw new Error(`Topping ${topping} not found`)
}
  } catch (error) {
     alert(`Ошибка  ${error.name}: ${error.message}`);
   }
 }

 calculatePrice() {
  let toppingPrice = 0;
  this._topping.forEach(element => {
    return toppingPrice += toppingProperties[element].price;
  });
  const price = sizesProperties[this._size].price + stuffingProperties[this._stuffing].price + toppingPrice;
  return price
 }

 calculateCalories() {
  let toppingCal = 0;
  this._topping.forEach(element => {
    return toppingCal += toppingProperties[element].cal;
  });
  const cal = sizesProperties[this._size].cal + stuffingProperties[this._stuffing].cal + toppingCal;
  return cal
 }
}


const hamburger = new Hamburger('large', 'potato' );
console.log(hamburger);
hamburger.topping = 'mayo';
hamburger.topping = 'spice';
hamburger.removeTopping = 'spice';
hamburger.stuffing = 'cheese';
hamburger.size = 'small';
console.log("Price: %f", hamburger.calculatePrice());
console.log("Calories: %f", hamburger.calculateCalories());


function getKeyByValue(object, value) {
  return Object.keys(object).find(key => object[key] === value);
}