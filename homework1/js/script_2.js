var hamburgerProperties = {
  SIZE_SMALL: {
    name:'small',
    price: 50,
    cal: 20,
  },
  SIZE_LARGE: {
    name:'large',
    price: 100,
    cal: 40,
  },
  STUFFING_CHEESE: {
    name:'cheese',
    price: 10,
    cal: 20,
  },
  STUFFING_SALAD: {
    name: 'salad',
    price: 20,
    cal: 5,
  },
  STUFFING_POTATO: {
    name: 'potato',
    price: 15,
    cal: 10,
  },
  TOPPING_MAYO: {
    name: 'mayo',
    price: 20,
    cal: 15,
  },
  TOPPING_SPICE: {
    name: 'spice',
    price: 15,
    cal: 0,
  }
}

function Hamburger(size, stuffing) {
  this._size = size;
  this._stuffing = stuffing;
  this._toppping = [];

  if (this._size === undefined) {
    throw new SyntaxError("Incorrect _size")
  };
  if (this._stuffing === undefined) {
    throw new SyntaxError("Incorrect _stuffing")
  };
}

Hamburger.prototype.addTopping = function (topping) {
  if (this._toppping.indexOf(topping) === -1) {
    this._toppping = this._toppping.concat(topping);
  } else {
    throw new SyntaxError("Dublicate topping");
  }
};

// Hamburger.prototype.removeTopping = function (topping) {
//   var toppingIndex = this._toppping.indexOf(topping.name);
//   if (toppingIndex !== -1) {
//     this._toppping.splice(toppingIndex, 1);
//   } else {
//     throw new SyntaxError(`Topping ${topping.name} not found`)
//   }
// };

Hamburger.prototype.calculatePrice = function () {
  var toppingPrice = 0;
  this._toppping.forEach(element => {
    toppingPrice += element.price;
    console.log(toppingPrice);
  });
  var price = this._size.price + this._stuffing.price + toppingPrice;
  return price
};




console.log(hamburgerProperties.hasOwnProperty('name'));
var hamburger = new Hamburger(hamburgerProperties.SIZE_SMALL, hamburgerProperties.STUFFING_POTATO);
console.log(hamburger);
hamburger.addTopping(hamburgerProperties.TOPPING_MAYO);
hamburger.addTopping(hamburgerProperties.TOPPING_SPICE);
// hamburger.removeTopping(hamburgerProperties.TOPPING_SPICE);
// hamburger.addTopping(hamburgerProperties.TOPPING_SPICE);
console.log(hamburger.calculatePrice());;


