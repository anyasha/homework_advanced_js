/* Размеры, виды начинок и добавок */
var sizes = {
  SIZE_SMALL: 'small',
  SIZE_LARGE: 'large',
};

var sizesProperties = {
  SIZE_SMALL: {
    price: 50,
    cal: 20,
  },
  SIZE_LARGE: {
    price: 100,
    cal: 40,
  }
};

var availableStuffings = {
  STUFFING_CHEESE: 'cheese',
  STUFFING_SALAD: 'salad',
  STUFFING_POTATO: 'potato',
  STUFFING_MAYO: 'mayo',
  STUFFING_SPICE: 'spice'
};

var stuffingProperties = {
  STUFFING_CHEESE: {
    price: 10,
    cal: 20
  },
  STUFFING_SALAD: {
    price: 20,
    cal: 5
  },
  STUFFING_POTATO: {
    price: 15,
    cal: 10
  },
};

var availableToppings = {
  TOPPING_MAYO: 'mayo',
  TOPPING_SPICE: 'spice'
};

var toppingProperties = {
  TOPPING_MAYO: {
    price: 20,
    cal: 15
  },
  TOPPING_SPICE: {
    price: 15,
    cal: 0
  }
};


function Hamburger(size, stuffing) {
  try {
    if (arguments.length !== 2) {
      throw new Error('Invalid parameter number')
    }
    if (size !== 'large' && size !== 'small') {
      throw new Error('Invalid _size')
    }
    if (stuffing !== 'cheese' && stuffing !== 'salad' && stuffing !== 'potato') {
      throw new Error('Invalid _stuffing')
    }
  this._size = getKeyByValue(sizes, size);
  this._stuffing = getKeyByValue(availableStuffings, stuffing);
  this._toppping = [];
    if (this._size === undefined) {
      throw new SyntaxError("Incorrect _size")
    }
    if (this._stuffing === undefined) {
        throw new SyntaxError("Incorrect _stuffing")
      }
  } catch (error) {
    alert((`Error  ${error.name}: ${error.message}`))
  }
}


Hamburger.prototype.addTopping = function (topping) {
  try {
  if (this._toppping.indexOf(getKeyByValue(availableToppings, topping)) === -1) {
    this._toppping = this._toppping.concat(getKeyByValue(availableToppings, topping));
  } else {
    throw new SyntaxError("Dublicate topping");
  }
  function getKeyByValue(object, value) {
    return Object.keys(object).find(key => object[key] === value);
  }
 } catch (error) {
    alert((`Error  ${error.name}: ${error.message}`))
  }
};

Hamburger.prototype.removeTopping = function (topping) {
  try {
    var toppingIndex = this._toppping.indexOf(getKeyByValue(availableToppings, topping));
if (toppingIndex !== -1) {
    this._toppping.splice(toppingIndex, 1);
  } else {
    throw new SyntaxError(`Topping ${topping} not found`)
  }
  } catch (error) {
    alert((`Error  ${error.name}: ${error.message}`))
  }
};

Hamburger.prototype.getToppings = function () {
  return this._toppping
}

Hamburger.prototype.getStuffing = function () {
  return this._stuffing
}

Hamburger.prototype.getSize = function () {
  return this._size
}

Hamburger.prototype.calculatePrice = function () {
  var toppingPrice = 0;
  this._toppping.forEach(element => {
    return toppingPrice += toppingProperties[element].price;
  });
  var price = sizesProperties[this._size].price + stuffingProperties[this._stuffing].price + toppingPrice;
  return price
};

Hamburger.prototype.calculateCalories = function () {
  var toppingCal = 0;
  this._toppping.forEach(element => {
    return toppingCal += toppingProperties[element].cal;
  });
  var cal = sizesProperties[this._size].cal + stuffingProperties[this._stuffing].cal + toppingCal;
  return cal
};

var hamburger = new Hamburger('large', 'potato');
hamburger.addTopping('mayo');
hamburger.addTopping('spice');
// hamburger.removeTopping('spice');
console.log(hamburger);
console.log(hamburger.getToppings());
console.log(hamburger.getSize());
console.log(hamburger.getStuffing());
console.log(hamburger.calculatePrice());
console.log(hamburger.calculateCalories());


function getKeyByValue(object, value) {
  return Object.keys(object).find(key => object[key] === value);
}