class game {
  constructor(id) {
    const table = document.createElement('table');
    for (let row = 0; row < 9; row++) {
      const tr = document.createElement('tr');
      for (let cell = 0; cell < 9; cell++) {
        const td = document.createElement('td');
        tr.appendChild(td);
        td.innerHTML = row * 10 + cell + 1;
      }
      table.appendChild(tr);
    }
    const container = document.getElementById(id);
    container.append(table)
  }
}
const game1 = new game('wrapper');